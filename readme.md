# Fungible Development QuickNode Api Package
This package is created to make QuickNode Api calls more easy to use.
***
## Table of Contents:
1. [Installation](#installation)
2. [Usage](#usage)

***
## Installation:
***
Install the latest version of the package by running:
```
composer require fungible/quicknode-api
```
Publish the config by running:
```
php artisan vendor:publish
```
Then configure the quicknode.php config with your QuickNode api key.
***

## Usage:
***
To start using the api calls create an api file with the following code:
```php
    public $client;

    public function __construct()
    {
        $this->client = new QuickNode();
    }
```
Now you can create functions and call `$this->client->getContentByBlockHash($blockNumber, $subDomain)` or any other function provided below!
***