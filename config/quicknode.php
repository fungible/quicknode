<?php

return [
    'rpc_url' => [
        'algorand' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_ALGORAND_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ALGORAND_MAINNET_WSS', '')
            ],
            'testnet' => [
                'http_provider' => env('QUICKNODE_ALGORAND_TESTNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ALGORAND_TESTNET_WSS', '')
            ],
            'betanet' => [
                'http_provider' => env('QUICKNODE_ALGORAND_BETANET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ALGORAND_BETANET_WSS', '')
            ],
        ],
        'arbitrum' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_ARBITRUM_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ARBITRUM_MAINNET_WSS', '')
            ],
            'goerli_testnet' => [
                'http_provider' => env('QUICKNODE_ARBITRUM_GOERLI_TESTNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ARBITRUM_GOERLI_TESTNET_WSS', '')
            ],
            'rinkeby_testnet' => [
                'http_provider' => env('QUICKNODE_ARBITRUM_RINKEBY_TESTNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ARBITRUM_RINKEBY_TESTNET_WSS', '')
            ],
        ],
        'arbitrum_nova' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_ARBITRUM_NOVA_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ARBITRUM_NOVA_MAINNET_WSS', '')
            ],
        ],
        'avalanche' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_AVALANCHE_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_AVALANCHE_MAINNET_WSS', '')
            ],
            'fuji_testnet' => [
                'http_provider' => env('QUICKNODE_AVALANCHE_FUJI_TESTNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_AVALANCHE_FUJI_TESTNET_WSS', '')
            ],
        ],
        'binance_smart_chain' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_BINANCE_SMART_CHAIN_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_BINANCE_SMART_CHAIN_MAINNET_WSS', '')
            ],
            'testnet' => [
                'http_provider' => env('QUICKNODE_BINANCE_SMART_CHAIN_TESTNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_BINANCE_SMART_CHAIN_TESTNET_WSS', '')
            ],
        ],
        'celo' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_CELO_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_CELO_MAINNET_WSS', '')
            ],
        ],
        'ethereum' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_ETHEREUM_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ETHEREUM_MAINNET_WSS', '')
            ],
            'goerli' => [
                'http_provider' => env('QUICKNODE_ETHEREUM_GOERLI_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ETHEREUM_GOERLI_WSS', '')
            ],
            'ropsten' => [
                'http_provider' => env('QUICKNODE_ETHEREUM_ROPSTEN_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ETHEREUM_ROPSTEN_WSS', '')
            ],
            'kovan' => [
                'http_provider' => env('QUICKNODE_ETHEREUM_KOVAN_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ETHEREUM_KOVAN_WSS', '')
            ],
            'rinkeby' => [
                'http_provider' => env('QUICKNODE_ETHEREUM_RINKEBY_HTTP', ''),
                'wss_provider' => env('QUICKNODE_ETHEREUM_RINKEBY_WSS', '')
            ],
        ],
        'fantom' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_FANTOM_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_FANTOM_MAINNET_WSS', '')
            ],
        ],
        'gnosis' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_GNOSIS_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_GNOSIS_MAINNET_WSS', '')
            ],
        ],
        'harmony' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_HARMONY_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_HARMONY_MAINNET_WSS', '')
            ],
            'testnet' => [
                'http_provider' => env('QUICKNODE_HARMONY_TESTNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_HARMONY_TESTNET_WSS', '')
            ],
        ],
        'optimism' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_OPTIMISTIC_ETHEREUM_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_OPTIMISTIC_ETHEREUM_MAINNET_WSS', '')
            ],
            'goerli_testnet' => [
                'http_provider' => env('QUICKNODE_BINANCE_SMART_CHAIN_GOERLI_TESTNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_BINANCE_SMART_CHAIN_GOERLI_TESTNET_WSS', '')
            ],
            'kovan_testnet' => [
                'http_provider' => env('QUICKNODE_BINANCE_SMART_CHAIN_KOVAN_TESTNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_BINANCE_SMART_CHAIN_KOVAN_TESTNET_WSS', '')
            ],
        ],
        'polygon' => [
            'mainnet' => [
                'http_provider' => env('QUICKNODE_POLYGON_MAINNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_POLYGON_MAINNET_WSS', '')
            ],
            'mumbai_testnet' => [
                'http_provider' => env('QUICKNODE_POLYGON_TESTNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_POLYGON_TESTNET_WSS', '')
            ],
        ],
        'solana' => [
            'mainnet_beta' => [
                'http_provider' => env('QUICKNODE_SOLANA_MAINNET_BETA_HTTP', ''),
                'wss_provider' => env('QUICKNODE_SOLANA_MAINNET_BETA_WSS', '')
            ],
            'testnet' => [
                'http_provider' => env('QUICKNODE_SOLANA_TESTNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_SOLANA_TESTNET_WSS', '')
            ],
            'devnet' => [
                'http_provider' => env('QUICKNODE_SOLANA_DEVNET_HTTP', ''),
                'wss_provider' => env('QUICKNODE_SOLANA_DEVNET_WSS', '')
            ],
        ],
    ]
];
