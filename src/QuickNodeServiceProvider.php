<?php

namespace Fungible\QuickNodeApi;

use Illuminate\Support\ServiceProvider;

class QuickNodeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/quicknode.php' => config_path('quicknode.php')
        ]);
    }

    public function register()
    {
        $this->app->singleton(QuickNode::class, function() {
            return new QuickNode();
        });
    }
}
