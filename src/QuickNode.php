<?php

namespace Fungible\QuickNodeApi;

use Fungible\QuickNodeApi\Chains\Celo\Celo;
use Fungible\QuickNodeApi\Chains\Fantom\Fantom;
use Fungible\QuickNodeApi\Chains\Gnosis\Gnosis;
use Fungible\QuickNodeApi\Chains\Solana\Solana;
use Fungible\QuickNodeApi\Chains\Harmony\Harmony;
use Fungible\QuickNodeApi\Chains\Polygon\Polygon;
use Fungible\QuickNodeApi\Chains\Algorand\Algorand;
use Fungible\QuickNodeApi\Chains\Arbitrum\Arbitrum;
use Fungible\QuickNodeApi\Chains\Ethereum\Ethereum;
use Fungible\QuickNodeApi\Chains\Optimism\Optimism;
use Fungible\QuickNodeApi\Chains\Avalanche\Avalanche;
use Fungible\QuickNodeApi\Chains\ArbitrumNova\ArbitrumNova;
use Fungible\QuickNodeApi\Chains\BinanceSmartChain\BinanceSmartChain;
use Fungible\QuickNodeApi\Chains\TerraMoney\TerraMoney;

class QuickNode
{
    public $algorand;
    public $arbitrum;
    public $arbitrumNova;
    public $avalanche;
    public $binanceSmartChain;
    public $celo;
    public $ethereum;
    public $fantom;
    public $gnosis;
    public $harmony;
    public $optimism;
    public $polygon;
    public $solana;
    public $terraMoney;

    public function __construct()
    {
        $this->algorand = new Algorand();
        $this->arbitrum = new Arbitrum();
        $this->arbitrumNova = new ArbitrumNova();
        $this->avalanche = new Avalanche();
        $this->binanceSmartChain = new BinanceSmartChain();
        $this->celo = new Celo();
        $this->ethereum = new Ethereum();
        $this->fantom = new Fantom();
        $this->gnosis = new Gnosis();
        $this->harmony = new Harmony();
        $this->optimism = new Optimism();
        $this->polygon = new Polygon();
        $this->solana = new Solana();
        $this->terraMoney = new TerraMoney();
    }
}
