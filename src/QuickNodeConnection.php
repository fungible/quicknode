<?php

namespace Fungible\QuickNodeApi;

use Error;
use GuzzleHttp\Client;


class QuickNodeConnection extends Client
{
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * Makes api call to get data from the RPC
     *
     * @param string $requestType
     * @param string $chain
     * @param string $network
     * @param $data
     * 
     * @throws Exception
     * @throws Error
     * 
     * @return array|null
     */
    public function makeApiCall(string $requestType = 'get', string $chain, string $network, $data)
    {
        if(!in_array($requestType, [])){
            throw new Error("Request type is not a available function guzzle uses.");
        }

        $url = config('quicknode.rpc_url.' . $chain . '.' . $network . 'http_provider');
        if(empty($url)){
            throw new Error("Rpc_url has not been set for:" . $chain . " for network:" . $network);
        }

        try {
            $response = $this->{$requestType}($url, $data);
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }

        return $responseArray;
    }
}
