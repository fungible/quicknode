<?php

namespace Fungible\QuickNodeApi\Chains\Ethereum;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class Ethereum extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'ethereum';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
        'goerli',
        'ropsten',
        'kovan',
        'rinkeby',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
