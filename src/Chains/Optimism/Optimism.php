<?php

namespace Fungible\QuickNodeApi\Chains\Optimism;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class Optimism extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'optimism';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
        'goerli_testnet',
        'kovan_testnet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
