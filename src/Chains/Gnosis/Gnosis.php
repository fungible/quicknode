<?php

namespace Fungible\QuickNodeApi\Chains\Gnosis;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class Gnosis extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'gnosis';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
