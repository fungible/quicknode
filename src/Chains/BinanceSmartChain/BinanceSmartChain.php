<?php

namespace Fungible\QuickNodeApi\Chains\BinanceSmartChain;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class BinanceSmartChain extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'binance_smart_chain';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
        'testnet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
