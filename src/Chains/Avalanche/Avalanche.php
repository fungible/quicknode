<?php

namespace Fungible\QuickNodeApi\Chains\Avalanche;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class Avalanche extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'avalanche';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
        'fuji_testnet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
