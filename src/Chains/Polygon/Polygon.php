<?php

namespace Fungible\QuickNodeApi\Chains\Polygon;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class Polygon extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'polygon';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
        'mumbai_testnet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
