<?php

namespace Fungible\QuickNodeApi\Chains;

trait GlobalChainTrait {
    /**
     * Changes the network you wanna use for the respected chain
     *
     * @param string $network
     * @return bool;
     */
    public function changeNetwork(string $network): bool
    {
        if(in_array($network, $this->availableNetworks)){
            $this->currentNetwork = $network;
            return true;
        }
        return false;
    }
}