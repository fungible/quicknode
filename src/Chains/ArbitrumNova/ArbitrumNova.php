<?php

namespace Fungible\QuickNodeApi\Chains\ArbitrumNova;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class ArbitrumNova extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'arbitrum_nova';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
