<?php

namespace Fungible\QuickNodeApi\Chains\Celo;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class Celo extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'celo';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
