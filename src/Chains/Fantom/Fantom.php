<?php

namespace Fungible\QuickNodeApi\Chains\Fantom;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class Fantom extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'fantom';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
