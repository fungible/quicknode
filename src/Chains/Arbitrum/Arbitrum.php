<?php

namespace Fungible\QuickNodeApi\Chains\Arbitrum;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class Arbitrum extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'arbitrum';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
        'goerli_testnet',
        'rinkeby_testnet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
