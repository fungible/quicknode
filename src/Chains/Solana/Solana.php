<?php

namespace Fungible\QuickNodeApi\Chains\Solana;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class Solana extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'solana';
    private $currentNetwork = 'mainnet_beta';

    private $availableNetworks = [
        'mainnet_beta',
        'testnet',
        'devnet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
