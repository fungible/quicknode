<?php

namespace Fungible\QuickNodeApi\Chains\Harmony;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class Harmony extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'harmony';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
        'testnet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
