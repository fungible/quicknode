<?php

namespace Fungible\QuickNodeApi\Chains\Algorand;

use Fungible\QuickNodeApi\QuickNodeConnection;
use Fungible\QuickNodeApi\Chains\GlobalChainTrait;

class Algorand extends QuickNodeConnection
{
    use GlobalChainTrait;
    
    /** @var string chain */
    private $chain = 'algorand';
    private $currentNetwork = 'mainnet';

    private $availableNetworks = [
        'mainnet',
        'testnet',
        'betanet',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
